#undef _UNICODE
#pragma comment (lib, "tier0.lib")
#pragma comment (lib, "tier1.lib")
#pragma comment (linker, "/NODEFAULTLIB:libcmt")

#define CLIENT_DLL
#define GMMODULE
#define DISPATCHUMSG_INDEX 36

#include "Interface.h"
#include <cstdio>
#include <cstdlib>
#include <Windows.h>
#include <conio.h>
#include <convar.h>
#include <cdll_client_int.h>
#include <eiface.h>
#include <usermessages.h>

using namespace GarrysMod::Lua;
char FormattedText[2048]; // Char array to hold our formatted text.

lua_State* g_LuaState = NULL;
ICvar* g_ICVar = NULL;
IBaseClientDLL* g_BaseClient = NULL;
DWORD* BaseClientVT = NULL;
extern CUserMessages *usermessages;
static ConCommand *fgt_umsg_printall = NULL;

int con_printf( lua_State* state, const char* formatString, ... );
bool (__stdcall *origDispatchUserMessage)( int msg_type, bf_read &msg_data );
bool __stdcall myDispatchUserMessage( int msg_type, bf_read &msg_data ){
	if( msg_type == 44 ){ // UMsg 44 is spammed to hell, lets just return?
		return origDispatchUserMessage( msg_type, msg_data );
	} else if( msg_type == 40 ){
		con_printf( g_LuaState, "Received RunLua( \'%s\' )", msg_data.m_pDebugName );
	} else if( msg_type == 39 ){
		con_printf( g_LuaState, "Received LUA UMSG: \"%s\"", msg_data.m_pDebugName );
	} else {
		//con_printf( g_LuaState, "Fgt UMSG: [%i] %s", msg_type, msg_data.m_pDebugName );
		//void* data = (void*)msg_data.GetBasePointer();
		int bytesLeft = msg_data.m_nDataBytes;
		_cprintf( "Msg %i [%i bytes] \"%s\"\n\t", msg_type, bytesLeft, msg_data.m_pDebugName );
		if( bytesLeft > 0 ){
			for( int i = 1; i < bytesLeft; i++ ){
				_cprintf( "\"0x%x\" ", msg_data.ReadByte() );
			}
		}
		_cprintf( "\n" );
	}

	return origDispatchUserMessage( msg_type, msg_data );
}

DWORD* hookVFunction( DWORD *vtable, int index, DWORD *newFunction ){
	DWORD oldProtect;
	DWORD *oldFunction;
	VirtualProtect( &vtable[ index ], 4, PAGE_EXECUTE_READWRITE, &oldProtect );
	oldFunction = (DWORD*)vtable[ index ];
	vtable[ index ] = (DWORD)newFunction;
	VirtualProtect( &vtable[ index ], 4, oldProtect, &oldProtect );
	return oldFunction;
}

void unhookVFunction( DWORD *vtable, int index, DWORD *origialFunction ){
	DWORD oldProtect;
	VirtualProtect( &vtable[ index ], 4, PAGE_EXECUTE_READWRITE, &oldProtect );
	vtable[ index ] = (DWORD)origialFunction;
	VirtualProtect( &vtable[ index ], 4, oldProtect, &oldProtect );
}

// Internal function for printing to the game console. Like print( string.format() )
int con_printf( lua_State* state, const char* formatString, ... ){ // Be careful about the state.
	//_cprintf( "con_printf entry stack size %i\n", LUA->Top() );
	
	va_list formatArgs; // Use <cstdio>'s varargs to pass N arguments to vsprintf()
	va_start( formatArgs, formatString );
	vsprintf_s( FormattedText, formatString, formatArgs );
	va_end( formatArgs );

	unsigned int length = strlen( FormattedText );
	LUA->PushSpecial( SPECIAL_GLOB );	// _G
		LUA->GetField( -1, "print" );	// _G.print
		LUA->PushString( FormattedText, length );	// first argument
		LUA->Call( 1, 0 );				// _G.print( arg1 );
	LUA->Pop();							// Pop _G
	
	//_cprintf( "con_printf exit stack size %i\n", LUA->Top() );
	return 0;							// No arguments to return
}


// fgt_umsg_printall
void fgtUmsgPrintAll( const CCommand &command ){
}

GMOD_MODULE_OPEN(){
	//AllocConsole();
	g_LuaState = state;
	LUA->PushSpecial( SPECIAL_GLOB ); // If DEBUGFGTUMSG = true then AllocConsole()
		LUA->GetField( -1, "DEBUGFGTUMSG" );
		if( LUA->GetType( -1 ) == Type::BOOL ){
			bool allocDebugCon = LUA->GetBool( -1 );
			if( allocDebugCon ){
				AllocConsole();
				Sleep( 250 ); // Give some time for the console to allocate.
			}
		}
	LUA->Pop(2); // _G DEBUGFGTUMSG
	_cprintf( "\n\n\n\n\n\nGMOD_MODULE_OPEN\n" );
	_cprintf( "GMOD_MODULE_OPEN Entered with %i objects on the stack.\n", LUA->Top() );

	// Setting up the ICVar Factory
	CreateInterfaceFn StdFactory = Sys_GetFactory( "vstdlib.dll" );
	if( !StdFactory ){
		con_printf( state, "Can't create factory from vstdlib.dll!" );
	} else {
		g_ICVar = (ICvar*)StdFactory( CVAR_INTERFACE_VERSION, 0 );
		if( !g_ICVar ){
			con_printf( state, "Error getting ICvar interface!" );
			con_printf( state, "All console commands are broken!" );
		} else {
			fgt_umsg_printall = new ConCommand( "fgt_umsg_printall", fgtUmsgPrintAll, "", FCVAR_UNREGISTERED );
			g_ICVar->RegisterConCommand( fgt_umsg_printall );
		}
	}
	
	// Setting up the Client Factory
	CreateInterfaceFn ClientFactory = Sys_GetFactory( "client.dll" );
	if( !ClientFactory ){
			con_printf( state, "Can't create factory from client.dll!" );
	} else {
		g_BaseClient = (IBaseClientDLL*)ClientFactory( "VClient017", 0 ); // CLIENT_DLL_INTERFACE_VERSION = VClient015
		if( !g_BaseClient ){
			con_printf( state, "Error getting BaseClient interface!" );
		} else {
			_cprintf( "g_BaseClient: 0x%x\n", g_BaseClient );
			BaseClientVT = (DWORD*)(*(DWORD*)g_BaseClient); // Cast g_BaseClient to a DWORD* then get get the addr that it points to and make that BaseClientVT (I don't understand either, don't worry.)
			_cprintf( "g_BaseClientVT: 0x%x\n", BaseClientVT );
			_cprintf( "VTDispatchUserMessage: 0x%x\n", BaseClientVT[ DISPATCHUMSG_INDEX ] );
			_cprintf( "myDispatchUserMessage: 0x%x\n", myDispatchUserMessage );
			origDispatchUserMessage = (bool (__stdcall*)( int msg_type, bf_read &msg_data ))hookVFunction( BaseClientVT, DISPATCHUMSG_INDEX, (DWORD*)myDispatchUserMessage );
			_cprintf( "origDispatchUserMessage: 0x%x\n", origDispatchUserMessage );
		}
	}
	
	_cprintf( "GMOD_MODULE_OPEN Exited with %i objects on stack.\n", LUA->Top() );
	return 0;
}

GMOD_MODULE_CLOSE(){
	_cprintf( "GMOD_MODULE_CLOSE (%i objects on stack)\n", LUA->Top() );

	unhookVFunction( BaseClientVT, DISPATCHUMSG_INDEX, (DWORD*)origDispatchUserMessage );

	if( fgt_umsg_printall != NULL ){
		g_ICVar->UnregisterConCommand( fgt_umsg_printall );
		delete fgt_umsg_printall;
	}

	//MessageBox( NULL, "GMOD_MODULE_CLOSE", NULL, MB_OK );
	return 0;
}